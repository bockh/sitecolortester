# Site Color Tester

This project contains boiler plate for color design tokens and applies them to a site.
Choose a primary color and the design tokens will adjust to give ten shades of the primary and the triadic complements.

This is a WIP.
Feel free to contribute.
I'll probably drop anything in here if it is not an overt security risk.
Future additions I'm considering adding include

- Working color picker
- Translation from hex or RGB to the HSL format needed for math
- A fully designed example site to show the components in context
- A component layout site with
  - cards
  - buttons
  - borders
  - gradients
  - etc.

